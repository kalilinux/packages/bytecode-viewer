Source: bytecode-viewer
Section: utils
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.7.0
Homepage: https://github.com/Konloch/bytecode-viewer
Vcs-Browser: https://gitlab.com/kalilinux/packages/bytecode-viewer
Vcs-Git: https://gitlab.com/kalilinux/packages/bytecode-viewer.git

Package: bytecode-viewer
Architecture: all
Depends: ${misc:Depends},
         default-jre,
         java-wrappers,
Description: Java 8+ Jar & Android APK Reverse Engineering Suite
 This package contains Bytecode Viewer (BCV). It is an Advanced Lightweight
 Java Bytecode Viewer, GUI Java Decompiler, GUI Bytecode Editor, GUI Smali, GUI
 Baksmali, GUI APK Editor, GUI Dex Editor, GUI APK Decompiler, GUI DEX
 Decompiler, GUI Procyon Java Decompiler, GUI Krakatau, GUI CFR Java
 Decompiler, GUI FernFlower Java Decompiler, GUI DEX2Jar, GUI Jar2DEX, GUI
 Jar-Jar, Hex Viewer, Code Searcher, Debugger and more.
 .
 There is also a plugin system that will allow you to interact with the loaded
 classfiles, for example you can write a String deobfuscator, a malicious code
 searcher, or something else you can think of. You can either use one of the
 pre-written plugins, or write your own. It supports groovy scripting. Once a
 plugin is activated, it will execute the plugin with a ClassNode ArrayList of
 every single class loaded in BCV, this allows the user to handle it completely
 using ASM.
 .
 It's currently being maintained and developed by Konloch.
